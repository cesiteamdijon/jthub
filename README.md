# Configuration serveur

Lancer sur localhost:8080/SpringTP/

# Configuration database

Dans le fichier **\SpringTP\src\main\webapp\WEB-INF\web.xml** : 

```xml
<env-entry> 
    <env-entry-name>properties-directory</env-entry-name>
    <env-entry-value>D:/eclipse/SpringTP/</env-entry-value> 
    <env-entry-type>java.lang.String</env-entry-type>
</env-entry>
```

 Remplacer le paramètre env-entry-value par le chemin absolu du projet.