package com.formation.cesi.models.statics;

import java.util.ArrayList;

import com.formation.cesi.models.Cart;

public class ClientCarts {
	static private int GlobalId = 0;

	public static int GetNewId() {
		return ClientCarts.GlobalId++;
	}
	
	public static ArrayList<Cart> Carts = new ArrayList<Cart>();
}
