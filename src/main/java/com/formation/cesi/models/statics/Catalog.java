package com.formation.cesi.models.statics;

import java.util.ArrayList;

import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;

public class Catalog {
	static private int GlobalId = 0;
	
	public static int GetNewId() {
		return Catalog.GlobalId++;
	}
	static public ArrayList<Product> Products = new ArrayList<Product>() {
		{
			
			add(new Product(Catalog.GetNewId(),
					"Costume sherif",
					"Entrez dans la peau de d'une Shérif de Western le temps d'une soirée avec ce très sexy costume de Cow Girl Sheriffia par Obsessive !\r\n"
							+ "\r\n"
							+ "La robe échancrée sur les hanches et le dos à la manière d'un soutien-gorge pour un style sexy !\r\n"
							+ "\r\n"
							+ "Son tissu noir brillant légèrement extensible est bordé de magnifiques franches de Cow-Boy sur le bas et le décolleté.\r\n"
							+ "\r\n"
							+ "Une paire de manchettes à franches assortie rappelle magnifiquement le style de la robe et apporte un coté Cow-Girl assumé !\r\n"
							+ "\r\n"
							+ "Accessoires indispensables à toutes les Shérifs de l'Ouest, ce déguisement coquin compte également un foulard marron satiné ainsi qu'un Chapeau de Cow-Boy ornée de la célèbre étoile de Shérif.",
					"1.jpg",
					50f,
					Category.Female));
			add(new Product(Catalog.GetNewId(),
					"Mystim Rodeo Robin",
					"Mystim Rodeo Robin se compose de deux anneaux ajustables en silicone, permettant une électrostimulation du pénis. Les sensations de picotements provoqué par l'électricité légère et non dangereuse provoque un plaisir sensuel hors du commun.\r\n" + 
					"\r\n" + 
					"Le premier anneau se place autour du gland et le second se place soit à la base de la verge soit autour des testicules. Un cordon est livré pour connecter les anneaux à un appareil d'électrostimulation Mystim (vendu séparément).\r\n" + 
					"\r\n" + 
					"Mystim est une marque allemande haut de gamme et un gage de qualité, spécialisée en électro-sexe.",
					"2.jpg", 
					35.50f,
					Category.Male));
			add(new Product(Catalog.GetNewId(),
					"Cagoule wetlook",
					"Cette cagoule à la matière brillante à l'effet \"wetlook\", recouvre totalement votre visage à l'exclusion de la bouche.\r\n" + 
					"\r\n" + 
					"Accessoire idéal pour répondre au désir de votre maître les yeux fermés, sa matière extensible vous assure une adaptation aux courbes de votre visage.\r\n" + 
					"\r\n" + 
					"En outre, la doublure du cou est équipé d'un élastique afin de vous garantir une tenue parfaite de la cagoule même pendant les jeux de soumission les plus extrême !",
					"3.jpg",
					25f,
					Category.Mixte));
			add(new Product(Catalog.GetNewId(),
					"Spéculum Vaginal",
					"Spéculum Vaginal fabriqué en acier inoxydable pour pratiques SM ou jeux médicaux. Écartez les deux branches pour ouvrir le spéculum vaginal et maintenez le ouvert grâce à ses vis de serrage.\r\n" + 
					"\r\n" + 
					"Longueur d'insertion maximum de 8.8 cm. Largeur maximale d'ouverture 9,6 cm env.\r\n" + 
					"\r\n" + 
					"Nous vous recommandons l'utilisation d'un lubrifiant anal. Désinfectez et nettoyez bien votre sextoy avant et après chaque utilisation avec un nettoyant sextoys adapté.",
					"4.jpg",
					40f,
					Category.Female));
			add(new Product(Catalog.GetNewId(),
					"Masturbateur 3FAP",
					"Si vous recherchez un masturbateur qui se démarque de la concurrence, le masturbateur 3FAP est ce qu'il vous faut. \r\n" + 
					"\r\n" + 
					"\r\n" + 
					"3FAP est le premier masturbateur triple orifices qui ne nécessite pas de changer de manchon, les 3 orifices sont alignés sur le même appareil ce qui permet de passer de l'un à l'autre rapidement et simplement. D'autres jouets sexuels masculins vous obligent à changer continuellement les manchons pour avoir des sensations différentes, mais pas 3FAP! La conception unique 3 en 1 de 3FAP vous permet d'utiliser 3 orifices, 3 textures différentes et 3 niveaux d'aspiration, le tout en un seul produit.\r\n" + 
					"\r\n" + 
					"Chacun des 3 orifices a été scanné à partir d'un gagnant du Concours World Most Beautiful Vagina Contest, une compétition mondiale pour trouver les orifices les plus intéressants au monde. L'assemblage et le nettoyage du 3FAP sont facile grâce à sa conception unique en trois parties. Réalisé en TPE sans phtalate, 3FAP mesure 23 x 9 x 20 cm.\r\n" + 
					"\r\n" + 
					"Le sexshop RueDesPlaisirs vous recommande l'utilisation d'un lubrifiant à base d'eau. Nettoyez bien votre sextoy avant et après chaque utilisation avec un nettoyant sextoys adapté.",
					"5.jpg",
					90f,
					Category.Male));
		}
	};
}
