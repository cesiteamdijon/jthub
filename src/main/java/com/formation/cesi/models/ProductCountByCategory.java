package com.formation.cesi.models;

// Bof comme classe mais je vois pas cmt faire autrement atm
public class ProductCountByCategory {
	private  Category Category;
	private  int Count;
	public Category getCategory() {
		return Category;
	}
	public void setCategory(Category category) {
		Category = category;
	}
	public int getCount() {
		return Count;
	}
	public void setCount(int count) {
		Count = count;
	}
	
	public ProductCountByCategory(Category category, int count) {
		this.Category = category;
		this.Count = count;
	}
	
	public void increment() {
		this.Count++;
	}
}
