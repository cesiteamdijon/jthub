package com.formation.cesi.models;

public class Product {

    private int Id;



    private String Title;



    private String Description;



    private String ImageUrl;



    private Category Category;



    private float Price;



    public float getPrice() {
        return Price;
    }



    public void setPrice(float price) {
        Price = price;
    }



    public int getId() {
        return Id;
    }



    public void setId(int id) {
        Id = id;
    }



    public String getTitle() {
        return Title;
    }



    public void setTitle(String title) {
        Title = title;
    }



    public String getDescription() {
        return Description;
    }



    public void setDescription(String description) {
        Description = description;
    }



    public String getEmphasis() {
        return getDescription().substring(0, 100) + "...";
    }


    public String getImageUrl() {
        return ImageUrl;
    }



    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }



    public Category getCategory() {
        return Category;
    }



    public void setCategory(Category category) {
        Category = category;
    }



    public Product(int id, String title, String description, String imageUrl, float price, Category category) {
        this.Id = id;
        this.Title = title;
        this.Description = description;
        this.ImageUrl = imageUrl;
        this.Price = price;
        this.Category = category;
    }


}
