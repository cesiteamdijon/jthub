package com.formation.cesi.models;

public class CartItem {
	private int Id;
	// pas fou de mettre directement le produit mais flemme de faire mieux pour le moment :p
	private Product Product;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public Product getProduct() {
		return Product;
	}
	public void setProduct(Product product) {
		this.Product = product;
	}
	
	public CartItem(int id, Product product) {
		this.Id = id;
		this.Product = product;
	}
}
