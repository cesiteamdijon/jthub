package com.formation.cesi.models;

public class Client {
	private String Login;
	private String Password;
	public String getLogin() {
		return Login;
	}
	public void setLogin(String login) {
		Login = login;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public Client(String login, String password) {
		this.Login = login;
		this.Password = password;
	}
}
