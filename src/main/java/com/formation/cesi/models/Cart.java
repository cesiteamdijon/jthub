package com.formation.cesi.models;

import java.util.ArrayList;

import com.formation.cesi.models.statics.Catalog;

public class Cart {
	private int Id;
	private Client Client;
	private ArrayList<CartItem> CartItems;
	private boolean IsValidated;
	
	private int ItemIdIncrement;
	
	public int GetNewItemId() {
		return this.ItemIdIncrement++;
	}
	
	public boolean isIsValidated() {
		return IsValidated;
	}

	public void setIsValidated(boolean isValidated) {
		IsValidated = isValidated;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Client getClient() {
		return Client;
	}

	public void setClient(Client client) {
		Client = client;
	}

	public ArrayList<CartItem> getCartItems() {
		return CartItems;
	}

	public void setCartItems(ArrayList<CartItem> cartItems) {
		CartItems = cartItems;
	}

	public Cart() {
		this.ItemIdIncrement = 0;
		this.CartItems = new ArrayList<CartItem>();
		this.IsValidated = false;
	}
	
	public Cart(int id, Client client) {
		this();
		this.Id = id;
		this.Client = client;
	}
	
	public Cart(int id, Client client, ArrayList<CartItem> cartItems) {
		this(id, client);
		this.CartItems = cartItems;
	}
	
	public void AddItem(CartItem item) {
		this.CartItems.add(item);
	}
	
	public void RemoveItem(int itemId) {
		CartItem toRemove = null;
		for (CartItem c : this.CartItems) {
			if (c.getId() == itemId)
				toRemove = c;
		}
		if (toRemove != null) {
			this.CartItems.remove(toRemove);
		}
	}
	
	public float getTotalPrice() {
		float total = 0;
		for (CartItem c : this.CartItems) {
			total += c.getProduct().getPrice();
		}
		return total;
	}
}
