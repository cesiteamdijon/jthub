package com.formation.cesi.repositories;

import org.springframework.stereotype.Service;

import com.formation.cesi.irepositories.ICartRepository;
import com.formation.cesi.models.Cart;
import com.formation.cesi.models.CartItem;
import com.formation.cesi.models.Client;
import com.formation.cesi.models.statics.ClientCarts;

@Service
public class CartRepository implements ICartRepository {
	public Cart Create(Client client) {
		Cart cart = new Cart(ClientCarts.GetNewId(), client);
		ClientCarts.Carts.add(cart);
		return cart;
	}

	// Pas d'utilité actuellement, les carts validés sont gardés
	// et la proprité IsValidated mise à true
	public void Delete(int cartId) {
		Cart cartToRemove = null;
		for (Cart c : ClientCarts.Carts) {
			if (c.getId() == cartId)
				cartToRemove = c;
		}	
		if (cartToRemove != null)
			ClientCarts.Carts.remove(cartToRemove);
	}

	// Le get devrait plutôt se faire via idClient mais il n'existe pas encore
	public Cart Get(int cartId) {
		for (Cart c : ClientCarts.Carts) {
			if (c.getId() == cartId /*&& c.getIsValidated == false*/)
				return c;
		}
		return null;
	}

	public Cart AddItem(int cartId, CartItem cartItem) {
		Cart cart = Get(cartId);
		cart.AddItem(cartItem);
		return cart;
	}

	public Cart RemoveItem(int cartId, int idItem) {
		Cart cart = Get(cartId);
		cart.RemoveItem(idItem);
		return cart;
	}

	public void Validate(int cartId) {
		Get(cartId).setIsValidated(true);
	}


}
