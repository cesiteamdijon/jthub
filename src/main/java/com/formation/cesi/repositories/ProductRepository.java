package com.formation.cesi.repositories;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.formation.cesi.irepositories.IProductRepository;
import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;
import com.formation.cesi.models.statics.Catalog;
@Service
public class ProductRepository implements IProductRepository {

	public Product GetProduct(int idProduct) {
		for (Product p : Catalog.Products) {
			if (p.getId() == idProduct)
				return p;
		}
		return null;
	}

	public ArrayList<Product> GetAllProducts() {
		return Catalog.Products;
	}

	public void AddProduct(Product p) {
		Catalog.Products.add(p);
	}

	public ArrayList<Product> GetByCategory(Category category) {
		ArrayList<Product> list = new ArrayList<Product>();
		for (Product p :  Catalog.Products) {
			if (p.getCategory() == category) {
				list.add(p);
			}
		}
		return list;
	}

	public ArrayList<ProductCountByCategory> GetAllCountOfProductByCategory() {
		ArrayList<ProductCountByCategory> list = new ArrayList<ProductCountByCategory>();
		for (Product p :  Catalog.Products) {
			Category currentCategory = p.getCategory();
			boolean categoryNotInList = true;
			for (ProductCountByCategory pc : list) {
				if (pc.getCategory() == currentCategory) {
					pc.increment();
					categoryNotInList = false;
				}
					
			}
			if (categoryNotInList) {
				list.add(new ProductCountByCategory(currentCategory, 1));
			}
		}
		return list;
	}

}
