package com.formation.cesi.repositories;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.springframework.stereotype.Service;

import com.formation.cesi.exceptions.UnknownDatabaseException;
import com.formation.cesi.irepositories.IClientRepository;
import com.formation.cesi.models.Client;
@Service
public class ClientRepository extends BaseRepository implements IClientRepository{


	public Client setClient(String login, String password) {
		// TODO Auto-generated method stub
		return new Client(login, password);
	}

	public Client getClient() {
		// TODO Auto-generated method stub
		return null;
	}

	public void deleteClient() {
		// TODO Auto-generated method stub

	}

	public boolean hasClient(String login) {
		String sql = "SELECT username FROM User where username='" + login + "'";

        try (
				Connection conn = this.connect();
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery(sql);
			) {
            // loop through the result set
            while (rs.next()) {
            	return true;
            }
            return false;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
	}
	
	public boolean hasValidCredentials(String login, String password) {

		String sql = "SELECT username, password FROM User where username='" + login + "'";

        try {
        	Connection conn = this.connect();
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery(sql);
            // loop through the result set
            while (rs.next()) {
            	String passwordDb = rs.getString("password");
            	System.out.println("Password found:" + passwordDb);
            	if (passwordDb.compareTo(password) == 0) {
            		return true;
            	}
            }
            return false;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
	}

	@Override
	public Client addClient(String login, String password) throws UnknownDatabaseException{
		Client client = new Client(login, password);
        String sql = "INSERT INTO User(username,password) VALUES(?,?)";
        
        try {
        	Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, login);
            pstmt.setString(2, password);
            pstmt.executeUpdate();
            return client;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new UnknownDatabaseException(e.getMessage());
        }
	}

}
