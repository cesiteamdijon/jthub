package com.formation.cesi.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;

public abstract class BaseRepository {

    
    public static boolean initialize = false;
    
    private void initialize() {
    	try {
            Class.forName("org.sqlite.JDBC");
        } catch (Exception e) {
            this.initialize = false;
        }
    	this.initialize = true;
    }
    
    public Connection connect() {
    	
        
        Connection conn = null;
        String dbpath = "";
        try {
            Context ctx = new InitialContext();
            Context env = (Context) ctx.lookup("java:comp/env");
            dbpath = (String) env.lookup("properties-directory");
        } catch (Exception e) {
            return null;
        }
        try {

            // db parameters

//            String url = "jdbc:sqlite:D:/eclipse/SpringTP/jithub_db.db";
        	String url = "jdbc:sqlite:" + dbpath + "jithub_db.db";
        	// create a connection to the database
            System.out.println(url);
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");
            return conn;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }



    public BaseRepository() {
    	if (!this.initialize)
    		this.initialize();
    }
}
