package com.formation.cesi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.formation.cesi.exceptions.InvalidCredentialsException;
import com.formation.cesi.exceptions.InvalidProductException;
import com.formation.cesi.iservices.IProductService;
import com.formation.cesi.models.Product;

@Controller
@RequestMapping(value = {"/product/{idProduct}"})
public class ProductController {
	@Autowired
	IProductService productService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getProduct(final ModelMap pModel, @PathVariable(value="idProduct", required=true) final int idProduct) {
		
		
		try {
			Product p = this.productService.GetProduct(idProduct);
			pModel.addAttribute("product", p);
			return "product";
        } catch (InvalidProductException e) {
            return "catalog";
        }
		
		
	}
}
