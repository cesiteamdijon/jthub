package com.formation.cesi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.formation.cesi.iservices.IHelper;

@Controller
@RequestMapping(value = {"/index/{nom}", "/index", "/"})
public class IndexController {

	@Autowired
	IHelper helperService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String sayHello(final ModelMap pModel, @PathVariable(value="nom", required=false) final String nom) {
		if (nom == null)
		 pModel.addAttribute("nom", helperService.sayHello());
		else 
			pModel.addAttribute("nom", nom);
		return "index";
	}
}
