package com.formation.cesi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.formation.cesi.iservices.IAuthentificationService;

@Controller
@RequestMapping(value = { "//logout" })
public class LogoutController {
	@Autowired
	IAuthentificationService authentificationService;

	@RequestMapping(method = RequestMethod.GET)
	public String getLogout(final ModelMap pModel) {
		this.authentificationService.Logout();
		return "redirect:index";
	}

}
