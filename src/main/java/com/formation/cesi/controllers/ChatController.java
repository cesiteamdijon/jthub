package com.formation.cesi.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.formation.cesi.models.websocket.Message;
import com.formation.cesi.models.websocket.OutputMessage;

@Controller
@RequestMapping(value = {"/springchat"})
public class ChatController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String getCart(final ModelMap pModel) {
		return "chat";
	}
	
	@MessageMapping("/chat")
	@SendTo("/topic/messages")
	public OutputMessage send(Message message) throws Exception {
	    String time = new SimpleDateFormat("HH:mm").format(new Date());
	    return new OutputMessage(message.getFrom(), message.getText(), time);
	}
}
