package com.formation.cesi.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.formation.cesi.exceptions.InvalidCredentialsException;
import com.formation.cesi.iservices.IAuthentificationService;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = {"/login", "/login/{backurl}"})
public class LoginController {

    @Autowired
    IAuthentificationService authentificationService;



    @RequestMapping(method = RequestMethod.GET)
    public String getLogin(final ModelMap pModel) {

    	return "login";
    }



    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView postLogin(final ModelMap pModel, @RequestParam("login") String login, @RequestParam("password") String password, HttpServletRequest request) {

        try {

            this.authentificationService.Login(login, password);

            return new ModelAndView("redirect:/catalog");

        } catch (InvalidCredentialsException e) {

            return new ModelAndView("/login", pModel)
                .addObject("errorform", e.getMessage());
        }
    }
}
