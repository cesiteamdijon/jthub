package com.formation.cesi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.formation.cesi.exceptions.InvalidProductException;
import com.formation.cesi.iservices.IProductService;
import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = { "/catalog" })
public class CatalogController {
	@Autowired
	IProductService productService;

	@RequestMapping(method = RequestMethod.GET)
	public String getCatalog(final ModelMap pModel, @RequestParam(value = "category", required = false) Category category) {
		ArrayList<Product> products;
		if (category == null) {
			products = this.productService.GetAllProducts();
			pModel.addAttribute("products", products);
		} else {
			products = this.productService.GetProductByCategory(category);
			pModel.addAttribute("products", products);
		}
		
		ArrayList<ProductCountByCategory> productCountByCategory = this.productService.GetAllCountOfProductByCategory();
		pModel.addAttribute("ProductCountByCategory", productCountByCategory);
		return "catalog";
	}
}
