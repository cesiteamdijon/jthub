package com.formation.cesi.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.formation.cesi.exceptions.InvalidCredentialsException;
import com.formation.cesi.iservices.IHelper;
import com.formation.cesi.iservices.IRegistrationService;
import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;

@Controller
@RequestMapping(value = {"/register"})
public class RegisterController {
	
	@Autowired
	IRegistrationService registrationService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getRegistrationForm(final ModelMap pModel) {

		return "register";
	}
	
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView postRegistrationForm(final ModelMap pModel, @RequestParam("login") String login, @RequestParam("password") String password, HttpServletRequest request) {

        try {

        	this.registrationService.AddClient(login, password);

            return new ModelAndView("redirect:/catalog");

        } catch (InvalidCredentialsException e) {

            return new ModelAndView("/register", pModel)
                .addObject("errorform", e.getMessage());
        }
    }
}
