package com.formation.cesi.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.formation.cesi.exceptions.InvalidCredentialsException;
import com.formation.cesi.iservices.ICartService;
import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = { "/cart", "/cart/{action}", "/cart/{action}/{iditem}" })
public class CartController {
	@Autowired
	ICartService cartService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getCart(final ModelMap pModel) {
		this.cartService.Get();
		return "cart";
	}
	
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handleCart(final ModelMap pModel, @PathVariable(value="action", required=true) final String action, @PathVariable(value="iditem", required=false) final Integer iditem) {
    	if (action.equals("additem")) {
    		addItem(iditem);
    	} else if  (action.equals("removeitem")) {
    		removeItem(iditem);
    	} else if (action.equals("validate")) {
    		validate(pModel);
    	}  else {
    		// throw exception
    	}
        return new ModelAndView("redirect:/cart");
    }
    
    private void addItem(int productId) {
    	this.cartService.AddItem(productId);
    }
    
    private void removeItem(int itemId) {
    	this.cartService.RemoveItem(itemId);
    }
    
    private void validate(ModelMap pModel) {
    	this.cartService.Validate();
    	pModel.addAttribute("validateMessage", "Votre panier a bien �t� valid�");
    }
}
