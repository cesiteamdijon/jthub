package com.formation.cesi.exceptions;

public class UnknownDatabaseException extends Exception {

    public UnknownDatabaseException(String message) {
        super(message);
    }
}
