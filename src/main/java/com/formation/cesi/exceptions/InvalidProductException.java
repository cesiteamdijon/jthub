package com.formation.cesi.exceptions;

public class InvalidProductException extends Exception {
	public InvalidProductException(String message) {
		super(message);
	}
}
