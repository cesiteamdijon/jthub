package com.formation.cesi.irepositories;

import com.formation.cesi.exceptions.UnknownDatabaseException;
import com.formation.cesi.models.Client;

public interface IClientRepository {
	public boolean hasClient(String login);
	public boolean hasValidCredentials(String login, String password);
	public Client setClient(String login, String password);
	public Client getClient();
	public Client addClient(String login, String password) throws UnknownDatabaseException;
	public void deleteClient();
}
