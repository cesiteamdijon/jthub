package com.formation.cesi.irepositories;

import com.formation.cesi.models.Cart;
import com.formation.cesi.models.CartItem;
import com.formation.cesi.models.Client;

// Repository si on désire un jour garder en base les paniers
public interface ICartRepository {
	public Cart Create(Client client);
	public void Delete(int cartId);
	public Cart Get(int cartId); 
	public Cart AddItem(int cartId, CartItem cartItem);
	public Cart RemoveItem(int cartId, int idItem);
	
	public void Validate(int cartId);
}
