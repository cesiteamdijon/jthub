package com.formation.cesi.irepositories;

import java.util.ArrayList;

import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;

public interface IProductRepository {
	public Product GetProduct(int idProduct);
	public ArrayList<Product> GetAllProducts();
	public ArrayList<Product> GetByCategory(Category category);
	public ArrayList<ProductCountByCategory> GetAllCountOfProductByCategory();
	public void AddProduct(Product p);
}
