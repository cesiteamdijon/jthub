package com.formation.cesi.services;

import javax.servlet.http.HttpSession;

import com.formation.cesi.exceptions.InvalidCredentialsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.formation.cesi.irepositories.IClientRepository;
import com.formation.cesi.iservices.IAuthentificationService;
import com.formation.cesi.models.Client;

@Service
public class AuthentificationService implements IAuthentificationService{

	@Autowired
	IClientRepository clientRepository;
	@Autowired 
	private HttpSession httpSession;


	public void Login(String login, String password) throws InvalidCredentialsException {
		// TODO Auto-generated method stub 

		if (login.length() <= 4 || password.length() <= 4) {

			
			throw new InvalidCredentialsException("Le mot de passe n'est pas conforme");
		} else if (!this.clientRepository.hasValidCredentials(login, password)) {
			throw new InvalidCredentialsException("Les identifiants saisis ne sont pas conformes");
		}

		Client client = clientRepository.setClient(login, password);
		//Add to session
		this.httpSession.setAttribute("client", client);
	}

	public void Logout() {
		// TODO Auto-generated method stub
		this.httpSession.invalidate();
			
	}
}
