package com.formation.cesi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.cesi.exceptions.InvalidProductException;
import com.formation.cesi.irepositories.IProductRepository;
import com.formation.cesi.iservices.IProductService;
import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;
import com.formation.cesi.models.statics.Catalog;
@Service
public class ProductService implements IProductService {
	@Autowired
	IProductRepository productRepository;

	public Product GetProduct(int idProduct) throws InvalidProductException {
	
		Product p = this.productRepository.GetProduct(idProduct);
		if (p == null)
		{
			throw new InvalidProductException("Aucun produit avec l'id " + idProduct + "n'a été trouvé");
		}
		return p;
	}

	public ArrayList<Product> GetAllProducts() {
		// Balancer une exception si catalog == null ou catalog.length == 0 ?
		return Catalog.Products;
	}

	public void AddProduct(String title, String description, String imageUrl, float price, Category category)
			throws InvalidProductException {
		
		if (title == null || title.length() == 0) {
			throw new InvalidProductException("Le produit ne contient pas de titre");
		}
		if (description == null || description.length() == 0) {
			throw new InvalidProductException("Le produit ne contient pas de description");			
		}
		if (imageUrl == null || imageUrl.length() == 0) {
			throw new InvalidProductException("Le produit ne contient pas d'image");			
		}
		Product p = new Product(Catalog.GetNewId(), title, description, imageUrl, price, category);
		this.productRepository.AddProduct(p);
	}

	public ArrayList<Product> GetProductByCategory(Category category) {
		
		return this.productRepository.GetByCategory(category);
	}

	public ArrayList<ProductCountByCategory> GetAllCountOfProductByCategory() {
		
		return this.productRepository.GetAllCountOfProductByCategory();
	}
	
}
