package com.formation.cesi.services;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.formation.cesi.irepositories.ICartRepository;
import com.formation.cesi.irepositories.IProductRepository;
import com.formation.cesi.iservices.ICartService;
import com.formation.cesi.models.Cart;
import com.formation.cesi.models.CartItem;
import com.formation.cesi.models.Client;
import com.formation.cesi.models.Product;

@Service
public class CartService implements ICartService {
	@Autowired
	ICartRepository cartRepository;
	@Autowired
	IProductRepository productRepository;
	@Autowired 
	private HttpSession httpSession;
	// De la merde ?
	/*
	public static HttpSession session() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(true); // true == allow create
	}
	*/
	public void UpdateSessionCart(Cart cart) {
		//CartService.session().setAttribute("cart", cart);
		this.httpSession.setAttribute("cart", cart);
	}
	
	public Cart Get() {
		Cart cart = (Cart)this.httpSession.getAttribute("cart");
		if (cart != null && cart.isIsValidated() == false)
			return cart;
		Client client = (Client)this.httpSession.getAttribute("client");
		cart = cartRepository.Create(client);
		UpdateSessionCart(cart);
		return cart;
	}

	public void AddItem(int idProduct) {
		Product product = this.productRepository.GetProduct(idProduct);
		CartItem cartItem = new CartItem(this.Get().GetNewItemId(), product);
		Cart cart = this.cartRepository.AddItem(this.Get().getId(), cartItem);
		UpdateSessionCart(cart);
	}

	public void RemoveItem(int idItem) {
		Cart cart = this.cartRepository.RemoveItem(this.Get().getId(), idItem);
		UpdateSessionCart(cart);
	}

	
	public void Validate() {
		this.cartRepository.Validate(this.Get().getId());
		UpdateSessionCart(this.Get());
	}



}
