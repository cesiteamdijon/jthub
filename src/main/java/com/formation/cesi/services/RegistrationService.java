package com.formation.cesi.services;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formation.cesi.exceptions.InvalidCredentialsException;
import com.formation.cesi.exceptions.UnknownDatabaseException;
import com.formation.cesi.irepositories.IClientRepository;
import com.formation.cesi.iservices.IRegistrationService;
import com.formation.cesi.models.Client;
@Service
public class RegistrationService implements IRegistrationService{
	@Autowired
	IClientRepository clientRepository;
	@Autowired 
	private HttpSession httpSession;
	
	@Override
	public boolean HasClient(String login) {
		// TODO Auto-generated method stub
		return this.clientRepository.hasClient(login);
	}
	
	@Override
	public boolean IsCredentialsValid(String login, String password) {
		// TODO Auto-generated method stub
		if (login.length() >= 4 && password.length() >= 4)
			return true;
		return false;
	}
	
	
	@Override
	public void AddClient(String login, String password) throws InvalidCredentialsException {
		// TODO Auto-generated method stub
		if (!this.IsCredentialsValid(login, password)) {
			throw new InvalidCredentialsException("L'identifiant et le mot de passe doivent faire au moins 4 caractères");
		}
		if (HasClient(login)) {
			throw new InvalidCredentialsException("L'idenfiant est déjà utilisé.");
		}
		try {
			Client client = clientRepository.addClient(login, password);
			//Add to session
			this.httpSession.setAttribute("client", client);
		} catch (UnknownDatabaseException e ) {
			throw new InvalidCredentialsException("Impossible d'accéder à la base de données, veuillez reessayer plus tard.");
		} catch (Exception e ) {
			throw new InvalidCredentialsException("Une erreur inconnue s'est produite, arrêtez tout, eteigner votre machine et faites vous un chocolat chaud, un conseiller vous prendra en charge d'ici 2038.");
			
		}

	}



}
