package com.formation.cesi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;


@Configuration
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

    // Static Resource Config
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        // Css resource.
        registry.addResourceHandler("/images/**") //
                .addResourceLocations("/assets/images/").setCachePeriod(31556926);

        registry.addResourceHandler("/css/**") //
                .addResourceLocations("/assets/css/").setCachePeriod(31556926);

        registry.addResourceHandler("/fonts/**") //
                .addResourceLocations("/assets/fonts/").setCachePeriod(31556926);
        
        registry.addResourceHandler("/js/**") //
        .addResourceLocations("/assets/js/").setCachePeriod(31556926);
    }



    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}