package com.formation.cesi.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class AuthentificatedFilter
 */
@WebFilter("/cart/")
public class AuthentificatedFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthentificatedFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest) request).getSession();
		if (session.getAttribute("client") != null)
		{
			//((HttpServletRequest) request).setAttribute("islogged", true);
			//TODO
			chain.doFilter(request, response);
			
		} else {
			//((HttpServletRequest) request).setAttribute("islogged", false);
			//request.getRequestDispatcher("/WEB-INF/pages/admin.jsp").forward(request, response);
			//redirect vers userservlet
			((HttpServletResponse) response).sendRedirect(((HttpServletRequest) request).getContextPath() +  "/login");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
