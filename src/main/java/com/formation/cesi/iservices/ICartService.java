package com.formation.cesi.iservices;

import com.formation.cesi.models.Cart;
import com.formation.cesi.models.CartItem;
import com.formation.cesi.models.Client;

public interface ICartService {
	public Cart Get(); 
	public void UpdateSessionCart(Cart cart);
	public void AddItem(int idProduct);
	public void RemoveItem(int idItem);
	public void Validate();
}
