package com.formation.cesi.iservices;

import com.formation.cesi.exceptions.InvalidCredentialsException;

public interface IRegistrationService {
	public boolean IsCredentialsValid(String login, String password);
	public boolean HasClient(String login);
	public void AddClient(String login, String password) throws InvalidCredentialsException;
}
