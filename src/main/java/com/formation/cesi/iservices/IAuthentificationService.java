package com.formation.cesi.iservices;


import com.formation.cesi.exceptions.InvalidCredentialsException;

public interface IAuthentificationService {
	public void Login(String login, String password) throws InvalidCredentialsException;
	public void Logout();
}
