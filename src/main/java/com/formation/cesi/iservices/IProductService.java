package com.formation.cesi.iservices;

import java.util.ArrayList;

import com.formation.cesi.exceptions.InvalidProductException;
import com.formation.cesi.models.Category;
import com.formation.cesi.models.Product;
import com.formation.cesi.models.ProductCountByCategory;

public interface IProductService {
	public Product GetProduct(int idProduct) throws InvalidProductException ;
	public ArrayList<Product> GetAllProducts();
	public ArrayList<Product> GetProductByCategory(Category category);
	public ArrayList<ProductCountByCategory> GetAllCountOfProductByCategory();
	public void AddProduct(String title, String description, String imageUrl, float price, Category category) throws InvalidProductException;
}
