import './sockjs.js';
import './stomp.js';

export class Popup {

    constructor(element) {

        this.element = element;

        this.connectionElement = element.querySelector('.js-connect');
        this.responseElement = element.querySelector('.js-response');
        this.disconnectElement = element.querySelector('.js-disconnect');
        this.sendMessageElement = element.querySelector('.js-sendMessage');

        this.headerButton = element.querySelector('.js-header');
        this.contentElement = element.querySelector('.js-content');

        this.stompClient = null;

        this.messageBarElement = element.querySelector('.js-message-bar');
        this.connexionBarElement = element.querySelector('.js-connexion-bar');


        this.isConnected = false;

        this.isVisible = !this.contentElement.classList.contains('popup__content--hidden');
        this.headerButton.addEventListener('click', this.toggle.bind(this));

        this.connectionElement.addEventListener('click', this.connect.bind(this));
        // this.disconnectElement.addEventListener('click', this.disconnect.bind(this));
        this.sendMessageElement.addEventListener('click', this.sendMessage.bind(this));

        this.disconnect();

        this.handleFocus();
    }


    setConnected(isConnected) {

        var messageInput = this.messageBarElement.querySelector('input[type="text"].popup__input');

        this.isConnected = isConnected;
        this.responseElement.innerHTML = '';
        this.messageBarElement.classList.toggle('hidden', !isConnected);
        this.connexionBarElement.classList.toggle('hidden', isConnected);

        if (isConnected) {
            messageInput.focus();
        }
    }

    connect() {

        var self = this;

        var socket = new SockJS('/SpringTP/chat');
        this.stompClient = Stomp.over(socket);
        this.stompClient.connect({}, function(frame) {
            self.setConnected(true);
            console.log('Connected: ' + frame);
            self.stompClient.subscribe('/topic/messages', function(messageOutput) {
                self.showMessageOutput(JSON.parse(messageOutput.body));
            });
        });
    }

    disconnect() {
        if (this.stompClient != null) {
            this.stompClient.disconnect();
        }
        this.setConnected(false);
        console.log("Disconnected");
    }

    sendMessage() {

        var messageInput = this.messageBarElement.querySelector('input[type="text"].popup__input');

        var from = document.getElementById('from').value;
        var text = document.getElementById('text').value;
        this.stompClient.send("/app/chat", {}, JSON.stringify({
            'from': from,
            'text': text
        }));

        messageInput.value = null;
    }

    showMessageOutput(messageOutput) {

        var response = this.responseElement;

        var p = document.createElement('p');
        p.classList.add('popup__line')
        p.innerHTML = `
            <span class="popup__time">${messageOutput.time} |&nbsp;</span>  
            <span class="popup__author">${messageOutput.from} :&nbsp;</span>  
            <span class="popup__message">${messageOutput.text}</span>`;

        response.appendChild(p);

        var body = this.element.querySelector('.popup__body');

        body.scrollTop = body.scrollHeight;
    }

    toggle() {

        const connexionInput = this.connexionBarElement.querySelector('input[type="text"].popup__input');

        this.isVisible = !this.isVisible;

        this.contentElement.classList.toggle('popup__content--hidden', !this.isVisible);

        if (this.isVisible) {
            connexionInput.focus();
        }
    }

    handleFocus() {
    }
}