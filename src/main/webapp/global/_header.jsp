<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>


<c:set var="contextPath" value="${requestScope['javax.servlet.forward.context_path']}"/>

<header class="header">
    <nav class="nav nav--header container">

        <a class="logo" href="${contextPath}/index">
            <svg class="logo__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 253.66 61.47">
            <defs>
                <style>.cls-1 {
                    fill: #fff;
                }</style>
            </defs>
            <g data-name="Logo">
                <path class="cls-1"
                      d="M101.34,37.55a24.08,24.08,0,0,1-1,7.12,15,15,0,0,1-3,5.51,13.42,13.42,0,0,1-5,3.56,18,18,0,0,1-7,1.26,33.18,33.18,0,0,1-3.61-.19,30.9,30.9,0,0,1-3.7-.64l.57-6.32a1.91,1.91,0,0,1,.66-1.35A2.47,2.47,0,0,1,81,46a6.88,6.88,0,0,1,1.35.17,8.89,8.89,0,0,0,1.92.18A7.9,7.9,0,0,0,87,45.91,4.21,4.21,0,0,0,89,44.46a6.85,6.85,0,0,0,1.15-2.64,17.76,17.76,0,0,0,.39-4v-30h10.84Z"/>
                <path class="cls-1" d="M143,7.75v8.6H129.83V54.49H119V16.35H105.83V7.75Z"/>
                <path class="cls-1"
                      d="M147.47,54.49v-48h9.95V24.14a16.76,16.76,0,0,1,3.9-2.49,12.16,12.16,0,0,1,5-.94,11.69,11.69,0,0,1,4.83.94,10,10,0,0,1,3.54,2.64,11.34,11.34,0,0,1,2.18,4,16.66,16.66,0,0,1,.74,5.05V54.49h-9.95V33.35a5.8,5.8,0,0,0-1.12-3.8,4,4,0,0,0-3.3-1.37,6.93,6.93,0,0,0-3.08.71,12.11,12.11,0,0,0-2.76,1.89V54.49Z"/>
                <path class="cls-1"
                      d="M194,21.22V42.33a5.8,5.8,0,0,0,1.12,3.82,4.06,4.06,0,0,0,3.31,1.35,7.06,7.06,0,0,0,3.06-.68,11.61,11.61,0,0,0,2.78-1.89V21.22h9.94V54.49H208a2.35,2.35,0,0,1-2.44-1.67L205,50.9a19.17,19.17,0,0,1-2,1.68,12.38,12.38,0,0,1-2.2,1.28,13.08,13.08,0,0,1-2.52.84,13.58,13.58,0,0,1-2.95.3,11.66,11.66,0,0,1-4.83-.94A10.07,10.07,0,0,1,187,51.41a11.52,11.52,0,0,1-2.18-4,16.65,16.65,0,0,1-.73-5.07V21.22Z"/>
                <path class="cls-1"
                      d="M221.9,54.49v-48h9.95V24.75a14.82,14.82,0,0,1,4.21-2.93,12.92,12.92,0,0,1,5.54-1.11,10.47,10.47,0,0,1,4.81,1.12,10.86,10.86,0,0,1,3.82,3.29,16.57,16.57,0,0,1,2.52,5.31,25.64,25.64,0,0,1,.91,7.15,22.23,22.23,0,0,1-1,6.93A17.5,17.5,0,0,1,249.7,50a13.53,13.53,0,0,1-4.49,3.66A12.83,12.83,0,0,1,239.42,55a10.7,10.7,0,0,1-2.65-.29,9,9,0,0,1-2.15-.81,9.9,9.9,0,0,1-1.8-1.27A20.1,20.1,0,0,1,231.24,51l-.36,1.67a2.08,2.08,0,0,1-.82,1.41,2.76,2.76,0,0,1-1.55.39Zm16.23-26.31a8.56,8.56,0,0,0-2,.21,7.28,7.28,0,0,0-1.64.6,6.21,6.21,0,0,0-1.39,1,13.13,13.13,0,0,0-1.28,1.37V45.44a6.18,6.18,0,0,0,2.43,1.75,7.8,7.8,0,0,0,2.76.5,5.79,5.79,0,0,0,2.54-.55,5.09,5.09,0,0,0,2-1.75,9.2,9.2,0,0,0,1.33-3.12,19.73,19.73,0,0,0,.48-4.69,21.25,21.25,0,0,0-.38-4.42,9.24,9.24,0,0,0-1.08-2.91,4.14,4.14,0,0,0-1.65-1.58A4.63,4.63,0,0,0,238.13,28.18Z"/>
                <path class="cls-1"
                      d="M29.22.21a33,33,0,0,0-13,61.16.67.67,0,0,0,.94-.88,11.93,11.93,0,0,1-1.09-8.25c.88-3,3-4.91,5.67-6.36a4,4,0,0,0,1.64-1C23.2,39.53,23,34.24,22.85,29c-2.77-1.68-3.29-3-2.08-5.83a36.65,36.65,0,0,1,6.09-9.42c1.22-1.44,2.61-.68,4-.36.42-.13.87-.27,1.31-.44a3.53,3.53,0,0,1,4.07.93,32.87,32.87,0,0,1,5,6.28c2.05,3.33,2,4.79-.22,8.05.09,5.46.11,11,0,16.55,1,.53,2,1,2.93,1.5a12.34,12.34,0,0,1,5.54,5.66,10.46,10.46,0,0,1-.25,8.32.67.67,0,0,0,1,.84A33,33,0,0,0,29.22.21Z"/>
            </g>
        </svg>
        </a>

        <ul class="nav__group nav__group--fluid">
            <li class="nav__item"><a href="${contextPath}/catalog" class="nav__link">Catalogue</a></li>

            <c:if test="${!empty client}">
				<li class="nav__item">
                    <a href="${contextPath}/cart/" class="nav__link">Mon panier</a>
                </li>
            </c:if>

        </ul>
        <ul class="nav__group">

            <li class="nav__item">
                <a href="${contextPath}/${empty client ? 'login' : 'logout'}"
                   class="nav__link">${empty client ? 'Connexion' : 'Déconnexion'}
                </a>
            </li>

        </ul>
    </nav>
</header>