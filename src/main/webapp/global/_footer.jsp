<%@ page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<footer class="footer">
	<div class="footer__copyright">
		JThub&copy; An awesome project
	</div>
</footer>
<div class="popup js-popup">

	<header class="popup__header js-header">
		Chat communautaire
	</header>
	<div class="popup__content popup__content--hidden js-content">
		<div class="popup__body">
			<div class="popup__conversation js-response"></div>
		</div>
		<form action="javascript:void(0);" class="popup__footer js-connexion-bar">
			<input class="popup__input" type="text" id="from" placeholder="Entrez votre nom" />
			<input class="popup__input popup__input--submit js-connect" type="submit" value="Connexion">
		</form>

		<form action="javascript:void(0);" class="popup__footer js-message-bar">
			<input class="popup__input" type="text" id="text" placeholder="Ecrivez un message..." />
			<input class="popup__input popup__input--submit js-sendMessage" type="submit">
		</form>
		<%--<div>--%>
		<%--<button class="js-disconnect" disabled="disabled">--%>
		<%--Deconnexion</button>--%>
		<%--</div>--%>
	</div>
</div>

<script src="js/Popup.js" type="module"></script>
<script type="module">

    import { Popup } from './js/Popup.js';

    var popup = new Popup(document.querySelector('.js-popup'));

    console.log(popup);

</script>
