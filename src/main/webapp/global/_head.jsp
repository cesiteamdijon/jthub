<%@ page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>

<head>
    <meta charset="ISO-8859-1">
    <base href="/SpringTP/">
    <link rel="icon" type="image/png" href="images/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
</head>