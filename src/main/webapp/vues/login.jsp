<%@ page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:import url="../global/_head.jsp"/>
<body>
<%@include file="../global/_header.jsp" %>

<main class="content">

    <c:set var="contextPath"
           value="${requestScope['javax.servlet.forward.context_path']}"/>
    <c:choose>
        <c:when test="${not empty sessionScope.client}">
            <p>
                Vous êtes déjà connecté. Vous pouvez vous <a
                    href="${contextPath}\logout">déconnecter.</a>
            </p>
        </c:when>
        <c:otherwise>
            <form action="${contextPath}/login" class="form form--box" method="POST">
                <h1 class="form__title">Connectez vous</h1>
                <div class="form__group">
                    <label for="login">Identifiant</label>
                    <input type="text" class="form__input" id="login" name="login" placeholder="ex: Jacquie">
                </div>
                <div class="form__group">

                    <label for="login">Mot de passe</label>
                    <input type="password" class="form__input" name="password" placeholder="*********">
                    <c:if test="${not empty errorform }">
                        <p>${ errorform }</p>
                    </c:if>
                </div>
                <div class="form__group">
                    <a href="${contextPath}/register" class="link">Créer son compte</a>
                </div>

                <div class="form__group">
                    <input type="submit" class="form__input" value="Se connecter">
                </div>
            </form>
        </c:otherwise>
    </c:choose>

</main>

<%@include file="../global/_footer.jsp" %>
</body>
</html>