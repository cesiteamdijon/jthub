<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<c:import url="../global/_head.jsp" />

<title>Catalogue</title>
<body>
	<c:import url="../global/_header.jsp" />

	<main class="content">

		<section class="nav nav--standard container margin-top-16px">

			<!-- Categories existantes  -->
			<ul class="nav__group">
				<li class="nav__item">
					<a class="nav__link" href="${contextPath}/SpringTP/catalog">
						All
					</a>
				</li>

				<c:forEach items="${ProductCountByCategory}" var="pc">
					<li class="nav__item">
						<a class="nav__link" href="${contextPath}/SpringTP/catalog?category=${pc.getCategory()}">
								${pc.getCategory()} ( ${pc.getCount()} )
						</a>
					</li>
				</c:forEach>
			</ul>

		</section>

	<!-- List des produits -->
		<section class="container flex-grid">
			<c:forEach items="${products}" var="product">
				<article class="card">
					<header class="card__thumbnail">
						<a href="${contextPath}/SpringTP/product/${product.getId()}">
							<img class="card__img"
								 src="images/products/${product.getImageUrl()}"
								 alt="${product.getTitle() }">
						</a>
					</header>
					<div class="card__body">
						<div class="card__title">${product.getTitle() }</div>
						<div class="card__description">${product.getEmphasis() }</div>
					</div>
					<footer class="card__footer">
						<div class="card__price">${product.getPrice() }€</div>
						<a href="${contextPath}/SpringTP/product/${product.getId()}" class="link">En savoir plus...</a>
					</footer>
				</article>

			</c:forEach>
		</section>
	</main>

	<c:import url="../global/_footer.jsp" />

</body>
</html>