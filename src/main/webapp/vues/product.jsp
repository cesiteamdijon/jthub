<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<c:import url="../global/_head.jsp" />

<title>Fiche produit - ${ product.getTitle() }</title>
<body>
	<c:import url="../global/_header.jsp" />

	<main class="content">
		<div class="container">
				<h1>${product.getTitle() }</h1>
			<img style="width: 300px"
				src="images/products/${product.getImageUrl()}"
				alt="${product.getTitle() }">
			<p>Description :</p>
			<p>${ product.getDescription() }</p>
			<p>Prix :</p>
			<p>${ product.getPrice() }</p>
			<form action="${contextPath}/SpringTP/cart/additem/${product.getId()}" class="form" method="POST">
				<div class="form__group">
					<input type="submit" class="form__input" value="Ajouter au panier">
				</div>
			</form>
		</div>

	</main>

	<c:import url="../global/_footer.jsp" />

</body>
</html>