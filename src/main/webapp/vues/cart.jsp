<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<c:import url="../global/_head.jsp"/>

<title>Accueil</title>
<body>
<c:import url="../global/_header.jsp"/>

<main class="content">

    <div class="container">

        <c:if test="${ not empty validateMessage }">
            <p>${ validateMessage }</p>
        </c:if> <c:choose>

        <c:when test="${cart.getCartItems().size() != 0 }">
            <table class="table margin-top-16px">
                <colgroup>
                    <col width="auto">
                    <col width="auto">
                    <col width="160px">
                </colgroup>
                <thead class="table__header">
                <tr class="table__row">
                    <th class="table__cell table__cell--header">Nom du produit</th>
                    <th class="table__cell table__cell--header">Prix</th>
                    <th class="table__cell table__cell--header"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${cart.getCartItems()}" var="cartItem">
                    <tr class="table__row">
                        <td class="table__cell">${cartItem.getProduct().getTitle() }</td>
                        <td class="table__cell">${cartItem.getProduct().getPrice() } €</td>
                        <td class="table__cell table__cell--no-padding">
                            <form action="${contextPath}/SpringTP/cart/removeitem/${cartItem.getId()}"
                                  class="no-padding-margin"
                                  method="POST">
                                <input type="submit" class="table__action" value="X">
                            </form>
                        </td>
                    </tr>

                </c:forEach>
                <tr class="table__row table__row--recap">
                    <td class="table__cell">Total</td>
                    <td class="table__cell">${cart.getTotalPrice()} €</td>
                    <td class="table__cell table__cell--no-padding">
                        <form action="${contextPath}/SpringTP/cart/validate" class="no-padding-margin"
                              method="POST">
                            <input type="submit" class="table__action table__action--primary"
                                   value="Valider mon panier">
                        </form>
                    </td>
                </tr>
                </tbody>
            </table>
        </c:when>


        <c:otherwise>
            <c:if test="${empty validateMessage }">
                <p>Votre panier est vide.</p>
            </c:if>

        </c:otherwise>
    </c:choose>
    </div>
</main>

<c:import url="../global/_footer.jsp"/>

</body>
</html>